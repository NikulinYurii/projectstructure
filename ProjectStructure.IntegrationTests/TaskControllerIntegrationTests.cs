﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.IntegrationTests;
using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Task = ProjectStructure.Models.Task;

namespace ProjectStructure.Tests
{
    public class TaskControllerIntegrationTests : IClassFixture<CustomWebApplicateinFactory<Startup>>
    {
        private readonly HttpClient _client;
        private ProjectStructureDbContext _context;
        public TaskControllerIntegrationTests(CustomWebApplicateinFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        [Fact]
        public async System.Threading.Tasks.Task DellTask_ThenResponseNoContent()
        {
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var response = _client.DeleteAsync("task/dell/" + 1);

            Assert.Equal(HttpStatusCode.NoContent, response.Result.StatusCode);
        }
    }
}
