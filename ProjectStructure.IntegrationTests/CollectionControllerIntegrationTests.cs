﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Task = ProjectStructure.Models.Task;

namespace ProjectStructure.IntegrationTests
{
    public class CollectionControllerIntegrationTests : IClassFixture<CustomWebApplicateinFactory<Startup>>
    {
        private readonly HttpClient _client;
        private ProjectStructureDbContext _context;

        public CollectionControllerIntegrationTests(CustomWebApplicateinFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        [Fact]
        public void GetAllNotDoneTasks_WhenUserIdCorrect_ThenGetResult()
        {
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.InProcess,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"),
                    FinishedAt = null
                }
            };

            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var response = _client.GetAsync("collection/GetAllNotDoneTasks/1");
            Assert.Equal(HttpStatusCode.OK, response.Result.StatusCode);
        }
        [Fact]
        public void GetAllNotDoneTasks_WhenUserIdNotCorrect_ThenGetError()
        {
            var response = _client.GetAsync("collection/GetAllNotDoneTasks/-1");
            Assert.Equal(HttpStatusCode.OK, response.Result.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasks_NameLess45_ThenResponseWhithCode200()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var response = await _client.GetAsync("collection/GetUserTasks_NameLess45/1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
