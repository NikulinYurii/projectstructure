﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.IntegrationTests 
{
    public class UserControllerIntegrationTests : IClassFixture<CustomWebApplicateinFactory<Startup>>
    {
        private readonly HttpClient _client;
        private ProjectStructureDbContext _context;
        public UserControllerIntegrationTests(CustomWebApplicateinFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        [Fact]
        public void DellUser_ThenResponseNoContent()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            var response = _client.DeleteAsync("user/dell/" + 1);
            
            Assert.Equal(HttpStatusCode.NoContent, response.Result.StatusCode);
        }
    }
}
