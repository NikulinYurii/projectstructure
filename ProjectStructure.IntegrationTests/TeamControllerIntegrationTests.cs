﻿using Newtonsoft.Json;
using ProjectStructure.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.IntegrationTests
{
    public class TeamControllerIntegrationTests : IClassFixture<CustomWebApplicateinFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TeamControllerIntegrationTests(CustomWebApplicateinFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task AddTeam_ThenResponseWhithCode201()
        {
            var team = new TeamDTO()
            {
                Id = 1,
                Name = "TestTeam",
                CreatedAt = DateTime.Now
            };

            var jsonTeam = JsonConvert.SerializeObject(team);
            var resTeam = await _client.PostAsync("team/add", new StringContent(jsonTeam, Encoding.UTF8, "application/json"));

            var stringResponse = await resTeam.Content.ReadAsStringAsync();
            var created = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, resTeam.StatusCode);
        }
    }
}
