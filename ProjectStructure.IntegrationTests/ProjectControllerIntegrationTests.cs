using Newtonsoft.Json;
using ProjectStructure.Models.DTO;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicateinFactory<Startup>>
    {
        private readonly HttpClient _client;
        public ProjectControllerIntegrationTests(CustomWebApplicateinFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task AddProject_ThenResponseWhithCode201()
        {
            var team = new TeamDTO()
            {
                Id = 1,
                Name = "TestTeam",
                CreatedAt = DateTime.Now
            };
            var user = new UserDTO()
            {
                Id = 1,
                TeamId = 1,
                FirstName = "TestUserFirstName",
                LastName = "TestUserLastName",
                Email = "Test@email",
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now
            };
            var project = new ProjectDTO()
            {
                Id = 1,
                AuthorId = 1,
                TeamId = 1,
                Name = "TestProject",
                Description = "Testtesttesttest",
                Deadline = DateTime.Now,
                CreatedAt = DateTime.Now
            };

            var jsonTeam = JsonConvert.SerializeObject(team);
            var jsonUser = JsonConvert.SerializeObject(user);
            var jsonProject = JsonConvert.SerializeObject(project);

            var resTeam = await _client.PostAsync("team/add", new StringContent(jsonTeam, Encoding.UTF8, "application/json"));
            var resUser = await _client.PostAsync("user/add", new StringContent(jsonUser, Encoding.UTF8, "application/json"));
            var response = await _client.PostAsync("project/add", new StringContent(jsonProject, Encoding.UTF8, "application/json"));


            var r1 = await resTeam.Content.ReadAsStringAsync();
            var r2 = await resUser.Content.ReadAsStringAsync();

            var stringResponse = await response.Content.ReadAsStringAsync();
            var created = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }


    }
}
