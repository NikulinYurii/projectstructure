﻿using System;
using System.Collections.Generic;
using ProjectStructure_UI.Models.DTO;
using ProjectStructure_UI.Services;
using ThreadTask = System.Threading.Tasks;
using System.Timers;
using System.Linq;

namespace ProjectStructure_UI
{
    class Program
    {
        private static WebApiService _webApiService = new WebApiService();
        private static Random random = new Random();

        static void Main(string[] args)
        {
            var t = PrintServices();
            var s = AfterSelectService(t);
            ThreadTask.Task.WaitAll(AfterSelectServiceMenu(t, s.Result));
        }

        private static char PrintServices()
        {
            Console.Clear();
            Console.WriteLine("Shoos one:\n" +
                              "1 - Project  " +
                              "2 - Task  " +
                              "3 - Team  " +
                              "4 - User " +
                              "5 - Use Timer");
            var t = Console.ReadKey().KeyChar;

            if (t == '1' || t == '2' || t == '3' || t == '4' || t == '5')
            {
                return t;
            }
            else
            {
                return PrintServices();
            }    
        }
        private static char PrintServiceMenu(string service)
        {
            Console.Clear();
            Console.WriteLine("1 - Back  " +
                              $"2 - Dell {service}  " +
                              $"3 - Add {service}  " +
                              $"4 - Update {service}  " +
                              $"5 - Get all {service}s  " +
                              $"6 - Get {service} by Id  ");
            var t = Console.ReadKey().KeyChar;

            if (t == '1' || t == '2' || t == '3' || t == '4' || t == '5' || t == '6')
            {
                return t;
            }
            else
            {
                return PrintServiceMenu(service);
            }    
        }
        private static async ThreadTask.Task<char> AfterSelectService(char service)
        {
            char res = '1';
            switch (service)
            {
                case '1':
                    res = PrintServiceMenu("project");
                    break;
                case '2':
                    res = PrintServiceMenu("task");
                    break; 
                case '3':
                    res = PrintServiceMenu("team");
                    break; 
                case '4':
                    res = PrintServiceMenu("user");
                    break;
                case '5':
                    await PrintMarkRandomTaskWithDelay();
                    PrintServices();
                    break;
            }

            return res;
        }
        private static int PrintGetId(string operatin, string service, string? error)
        {
            int intRes = -1;
            
            Console.Clear();
            if(error != null)
                Console.WriteLine(error);
            
            Console.Write($"{operatin} {service}!\n" +
                          $"print {service} Id: ");
            var res = Console.ReadLine();
            if (!CanConvertToInt(res))
            {
                PrintGetId(operatin, service,"Id must be int!");
            }
            else
            {
                intRes = int.Parse(res);
            }
            
            return intRes;
        }
        private static bool CanConvertToInt(string text)
        {
            try
            {
                int.Parse(text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private static bool CanConvertToDateTime(string text)
        {
            try
            {
                Convert.ToDateTime(text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private static ProjectDTO CreateProjectDto(ProjectDTO? projectDto, string operation, string? error)
        {
            ProjectDTO pDto = new ProjectDTO();

            Console.Clear();
            if (error != null)
                Console.WriteLine(error);

            Console.WriteLine($"{operation} project!");

            if (projectDto != null && projectDto.Id != null)
            {
                pDto.Id = projectDto.Id;
                Console.WriteLine($"Id: {pDto.Id}");
            }
            else
            {
                Console.Write("Id: ");
                var stringId = Console.ReadLine();
                if (!CanConvertToInt(stringId))
                {
                    CreateProjectDto(pDto, operation, "Id must be int!");
                }
                else
                {
                    pDto.Id = int.Parse(stringId);
                }
            }

            if (projectDto != null && projectDto.AuthorId != null)
            {
                pDto.AuthorId = projectDto.AuthorId;
                Console.WriteLine($"AuthorId: {pDto.AuthorId}");
            }
            else
            {
                Console.Write("AuthorId: ");
                var authorStringId = Console.ReadLine();
                if (!CanConvertToInt(authorStringId))
                {
                    CreateProjectDto(pDto, operation,"AuthorId must be int!");
                }
                else
                {
                    pDto.AuthorId = int.Parse(authorStringId);
                }
            }
            
            if (projectDto != null && projectDto.TeamId != null)
            {
                pDto.TeamId = projectDto.TeamId;
                Console.WriteLine($"TeamId: {pDto.TeamId}");
            }
            else
            {
                Console.Write("TeamId: ");
                var teamStringId = Console.ReadLine();
                if (!CanConvertToInt(teamStringId))
                {
                    CreateProjectDto(pDto, operation,"TeamId must be int!");
                }
                else
                {
                    pDto.TeamId = int.Parse(teamStringId);
                }
            }

            Console.Write("Name: ");
            pDto.Name = Console.ReadLine();

            Console.Write("Description: ");
            pDto.Description = Console.ReadLine();

            if (projectDto != null && projectDto.Deadline != null)
            {
                pDto.Deadline = projectDto.Deadline;
                Console.WriteLine($"Deadline: {pDto.Deadline}");
            }
            else
            {
                Console.Write("Deadline: ");
                var deadlineString = Console.ReadLine();
                if (!CanConvertToDateTime(deadlineString))
                {
                    CreateProjectDto(pDto, operation,"Convert to datetime error!");
                }
                else
                {
                    pDto.Deadline = Convert.ToDateTime(deadlineString);
                } 
            }

            pDto.CreatedAt = DateTime.Now;

            return pDto;
        }
        private static TaskDTO CreateTaskDto(TaskDTO? taskDto, string operation, string? error)
        {
            TaskDTO tDto = new TaskDTO();

            Console.Clear();
            if (error != null)
                Console.WriteLine(error);

            Console.WriteLine($"{operation} task!");

            if (taskDto != null && taskDto.Id != null)
            {
                tDto.Id = taskDto.Id;
                Console.WriteLine($"Id: {tDto.Id}");
            }
            else
            {
                Console.Write("Id: ");
                var stringId = Console.ReadLine();
                if (!CanConvertToInt(stringId))
                {
                    CreateTaskDto(tDto, operation, "Id must be int!");
                }
                else
                {
                    tDto.Id = int.Parse(stringId);
                }
            }

            if (taskDto != null && taskDto.ProjectId != null)
            {
                tDto.ProjectId = taskDto.ProjectId;
                Console.WriteLine($"ProjectId: {tDto.ProjectId}");
            }
            else
            {
                Console.Write("ProjectId: ");
                var pStringId = Console.ReadLine();
                if (!CanConvertToInt(pStringId))
                {
                    CreateTaskDto(tDto, operation,"ProjectId must be int!");
                }
                else
                {
                    tDto.ProjectId = int.Parse(pStringId);
                }
            }
            
            if (taskDto != null && taskDto.PerformerId != null)
            {
                tDto.PerformerId = taskDto.PerformerId;
                Console.WriteLine($"PerformerId: {tDto.PerformerId}");
            }
            else
            {
                Console.Write("PerformerId: ");
                var pStringId = Console.ReadLine();
                if (!CanConvertToInt(pStringId))
                {
                    CreateTaskDto(tDto, operation,"PerformerId must be int!");
                }
                else
                {
                    tDto.PerformerId = int.Parse(pStringId);
                }
            }

            Console.Write("Name: ");
            tDto.Name = Console.ReadLine();

            Console.Write("Description: ");
            tDto.Description = Console.ReadLine();

            if (taskDto != null && taskDto.PerformerId != null)
            {
                tDto.State = taskDto.State;
                Console.WriteLine($"State: {tDto.State}");
            }
            else
            {
                Console.Write("State: ");
                var sStringId = Console.ReadLine();
                if (!CanConvertToInt(sStringId))
                {
                    CreateTaskDto(tDto, operation,"State must be int!");
                }
                else
                {
                    tDto.PerformerId = int.Parse(sStringId);
                }
            }
            
            if (taskDto != null && taskDto.CreatedAt != null)
            {
                tDto.CreatedAt = taskDto.CreatedAt;
                Console.WriteLine($"CreatedAt: {tDto.CreatedAt}");
            }
            else
            {
                Console.Write("CreatedAt: ");
                var cString = Console.ReadLine();
                if (!CanConvertToDateTime(cString))
                {
                    CreateTaskDto(tDto, operation,"Convert to datetime error!");
                }
                else
                {
                    tDto.CreatedAt = Convert.ToDateTime(cString);
                } 
            }

            if (taskDto != null && taskDto.FinishedAt != null)
            {
                tDto.FinishedAt = taskDto.FinishedAt;
                Console.WriteLine($"FinishedAt: {tDto.FinishedAt}");
            }
            else
            {
                Console.Write("FinishedAt: ");
                var fString = Console.ReadLine();
                if (!CanConvertToDateTime(fString))
                {
                    CreateTaskDto(tDto, operation,"Convert to datetime error!");
                }
                else
                {
                    tDto.FinishedAt = Convert.ToDateTime(fString);
                } 
            }
            
            return tDto;
        }
        private static TeamDTO CreateTeamDto(TeamDTO? teamDto, string operation, string? error)
        {
            TeamDTO tDto = new TeamDTO();

            Console.Clear();
            if (error != null)
                Console.WriteLine(error);

            Console.WriteLine($"{operation} team!");

            if (teamDto != null && teamDto.Id != null)
            {
                tDto.Id = teamDto.Id;
                Console.WriteLine($"Id: {tDto.Id}");
            }
            else
            {
                Console.Write("Id: ");
                var stringId = Console.ReadLine();
                if (!CanConvertToInt(stringId))
                {
                    CreateTeamDto(tDto, operation, "Id must be int!");
                }
                else
                {
                    tDto.Id = int.Parse(stringId);
                }
            }

            Console.Write("Name: ");
            tDto.Name = Console.ReadLine();

            if (teamDto != null && teamDto.CreatedAt != null)
            {
                tDto.CreatedAt = teamDto.CreatedAt;
                Console.WriteLine($"CreatedAt: {tDto.CreatedAt}");
            }
            else
            {
                Console.Write("CreatedAt: ");
                var cString = Console.ReadLine();
                if (!CanConvertToDateTime(cString))
                {
                    CreateTeamDto(tDto, operation,"Convert to datetime error!");
                }
                else
                {
                    tDto.CreatedAt = Convert.ToDateTime(cString);
                } 
            }

            return tDto;
        }
        private static UserDTO CreateUserDto(UserDTO? userDto, string operation, string? error)
        {
            UserDTO uDto = new UserDTO();

            Console.Clear();
            if (error != null)
                Console.WriteLine(error);

            Console.WriteLine($"{operation} user!");

            if (userDto != null && userDto.Id != null)
            {
                uDto.Id = userDto.Id;
                Console.WriteLine($"Id: {uDto.Id}");
            }
            else
            {
                Console.Write("Id: ");
                var stringId = Console.ReadLine();
                if (!CanConvertToInt(stringId))
                {
                    CreateUserDto(uDto, operation, "Id must be int!");
                }
                else
                {
                    uDto.Id = int.Parse(stringId);
                }
            }
            
            if (userDto != null && userDto.TeamId != null)
            {
                uDto.TeamId = userDto.TeamId;
                Console.WriteLine($"TeamId: {uDto.TeamId}");
            }
            else
            {
                Console.Write("TeamId: ");
                var teamStringId = Console.ReadLine();
                if (!CanConvertToInt(teamStringId))
                {
                    CreateUserDto(uDto, operation,"TeamId must be int!");
                }
                else
                {
                    uDto.TeamId = int.Parse(teamStringId);
                }
            }

            Console.Write("FirstName: ");
            uDto.FirstName = Console.ReadLine();
            
            Console.Write("LastName: ");
            uDto.LastName = Console.ReadLine();
            
            Console.Write("Email: ");
            uDto.Email = Console.ReadLine();

            if (userDto != null && userDto.RegisteredAt != null)
            {
                uDto.RegisteredAt = userDto.RegisteredAt;
                Console.WriteLine($"RegisteredAt: {uDto.RegisteredAt}");
            }
            else
            {
                Console.Write("RegisteredAt: ");
                var rString = Console.ReadLine();
                if (!CanConvertToDateTime(rString))
                {
                    CreateUserDto(uDto, operation,"Convert to datetime error!");
                }
                else
                {
                    uDto.RegisteredAt = Convert.ToDateTime(rString);
                } 
            }
            
            if (userDto != null && userDto.BirthDay != null)
            {
                uDto.BirthDay = userDto.BirthDay;
                Console.WriteLine($"BirthDay: {uDto.BirthDay}");
            }
            else
            {
                Console.Write("BirthDay: ");
                var bString = Console.ReadLine();
                if (!CanConvertToDateTime(bString))
                {
                    CreateUserDto(uDto, operation,"Convert to datetime error!");
                }
                else
                {
                    uDto.BirthDay = Convert.ToDateTime(bString);
                } 
            }

            return uDto;
        }

        private static void PrintResult<T>(T c)
        {
            Console.Clear();
            Console.WriteLine(c.ToString());
        }
        private static void PrintResult<T>(List<T> c)
        {
            Console.Clear();
            foreach (var el in c)
            {
                Console.WriteLine(el.ToString());
            }
        }
        
        private static async ThreadTask.Task AfterSelectServiceMenu(char service, char serviceMenu)
        {
            if (serviceMenu == '1')
                PrintServices();
            else
                switch (service)
                {
                    case '1':
                        switch (serviceMenu)
                        {
                            case '2':
                                await _webApiService.DellProject(PrintGetId("Delete","project", null));
                                break;
                            case '3':
                                PrintResult<ProjectDTO>(await _webApiService.AddProject(CreateProjectDto(null, "Create",null)));
                                break;
                            case '4':
                                PrintResult<ProjectDTO>(await _webApiService.UpdateProject(CreateProjectDto(null, "Update", null)));
                                break;
                            case '5':
                                PrintResult<ProjectDTO>(await _webApiService.GetProjects());
                                break;
                            case '6':
                                PrintResult<ProjectDTO>(await _webApiService.GetProject(PrintGetId("Get", "project", null)));
                                break;
                        }
                        break;
                    case '2':
                        switch (serviceMenu)
                        {
                            case '2':
                                await _webApiService.DellTask(PrintGetId("Delete", "task", null));
                                break;
                            case '3':
                                PrintResult<TaskDTO>(await _webApiService.AddTask(CreateTaskDto(null, "Create", null)));
                                break;
                            case '4':
                                PrintResult<TaskDTO>(await _webApiService.UpdateTask(CreateTaskDto(null, "Update", null)));
                                break;
                            case '5':
                                PrintResult<TaskDTO>(await _webApiService.GetTasks());
                                break;
                            case '6':
                                PrintResult<TaskDTO>(await _webApiService.GetTask(PrintGetId("Get", "task", null)));
                                break;
                        }
                        break; 
                    case '3':
                        switch (serviceMenu)
                        {
                            case '2':
                                await _webApiService.DellTeam(PrintGetId("Delete","team",null));
                                break;
                            case '3':
                                PrintResult(await _webApiService.AddTeam(CreateTeamDto(null, "Create", null)));
                                break;
                            case '4':
                                PrintResult(await _webApiService.UpdateTeam(CreateTeamDto(null, "Update", null)));
                                break;
                            case '5':
                                PrintResult(await _webApiService.GetTeams());
                                break;
                            case '6':
                                PrintResult(await _webApiService.GetTeam(PrintGetId("Get", "team", null)));
                                break;
                        }
                        break; 
                    case '4':
                        switch (serviceMenu)
                        {
                            case '2':
                                await _webApiService.DellUser(PrintGetId("Delete", "user", null));
                                break;
                            case '3':
                                PrintResult(await _webApiService.AddUser(CreateUserDto(null, "Create", null)));
                                break;
                            case '4':
                                PrintResult(await _webApiService.UpdateUser(CreateUserDto(null, "Update", null)));
                                break;
                            case '5':
                                PrintResult(await _webApiService.GetUsers());
                                break;
                            case '6':
                                PrintResult(await _webApiService.GetUser(PrintGetId("Get", "user", null)));
                                break;
                        }
                        break; 
                }
        }

        private static async ThreadTask.Task PrintMarkRandomTaskWithDelay()
        {
            while (true)
            {
                try
                {
                    var res = await MarkRandomTaskWithDelay(1000);
                    
                    Console.WriteLine();
                    Console.WriteLine($"Update to Done task Id: {res}");
                }
                catch(Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static ThreadTask.Task<int> MarkRandomTaskWithDelay(int timerDelay)
        {
            ThreadTask.Task.Delay(timerDelay);
            var tcs = new ThreadTask.TaskCompletionSource<int>();
            var aTimer = new System.Timers.Timer(1000);
            aTimer.Elapsed += (a, e) =>
            {
                List<TaskDTO> tasks = null;
                try
                {
                    tasks = _webApiService.GetTasks().Result;
                }
                catch(Exception ex)
                {
                    tcs.TrySetException(ex);
                    return;
                }

                var notDoneTasks = tasks.Select(t => t.State != Models.TaskState.Done).ToList();
                
                var task = tasks[random.Next(notDoneTasks.Count)];
                task.State = Models.TaskState.Done;
                task.FinishedAt = DateTime.Now;

                try
                {
                    tcs.TrySetResult(_webApiService.UpdateTask(task).Result.Id);
                }
                catch (Exception ex)
                {
                    tcs.TrySetException(ex);
                }

            };
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            return tcs.Task;
        }
    }
}