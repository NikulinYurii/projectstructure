﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure_UI.Models;
using ProjectStructure_UI.Models.DTO;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure_UI.Services
{
    public class WebApiService
    {
        private HttpClient client = new HttpClient();
        private readonly string _url = "https://localhost:5001/";

        public async ThreadTask.Task<List<ProjectDTO>> GetProjects()
        {
            string url = $"{_url}project/get/all";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<ProjectDTO>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<TaskDTO>> GetTasks()
        {
            string url = $"{_url}task/get/all";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<TaskDTO>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<TeamDTO>> GetTeams()
        {
            string url = $"{_url}team/get/all";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<TeamDTO>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<UserDTO>> GetUsers()
        {
            string url = $"{_url}user/get/all";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<UserDTO>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<ProjectDTO> GetProject(int id)
        {
            string url = $"{_url}project/get/{id}";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<ProjectDTO>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TaskDTO> GetTask(int id)
        {
            string url = $"{_url}task/get/{id}";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<TaskDTO>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TeamDTO> GetTeam(int id)
        {
            string url = $"{_url}team/get/{id}";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<TeamDTO>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<UserDTO> GetUser(int id)
        {
            string url = $"{_url}user/get/{id}";
            
            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<UserDTO>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }

        public async ThreadTask.Task<ProjectDTO> AddProject(ProjectDTO projectDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(projectDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}project/add", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ProjectDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TaskDTO> AddTask(TaskDTO taskDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(taskDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}task/add", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TaskDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TeamDTO> AddTeam(TeamDTO teamDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(teamDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}team/add", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TeamDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<UserDTO> AddUser(UserDTO userDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(userDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}user/add", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }

        public async ThreadTask.Task DellProject(int id)
        {
            var response =
                await client.DeleteAsync($"{_url}project/dell/" + id);
            
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task DellTask(int id)
        {
            var response =
                await client.DeleteAsync($"{_url}task/dell/" + id);
            
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task DellTeam(int id)
        {
            var response =
                await client.DeleteAsync($"{_url}team/dell/" + id);
            
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task DellUser(int id)
        {
            var response =
                await client.DeleteAsync($"{_url}user/dell/" + id);
            
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<ProjectDTO> UpdateProject(ProjectDTO projectDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(projectDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}project/update/{projectDto.Id}", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ProjectDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TaskDTO> UpdateTask(TaskDTO taskDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(taskDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}task/update/{taskDto.Id}", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TaskDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<TeamDTO> UpdateTeam(TeamDTO teamDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(teamDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}team/update/{teamDto.Id}", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TeamDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<UserDTO> UpdateUser(UserDTO userDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(userDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{_url}user/update/{userDto.Id}", content);
            
            if (response.StatusCode == HttpStatusCode.Created)
            {
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDTO>(res);
            }
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }

        public async ThreadTask.Task<Dictionary<Project, int>> GetUserTasksCount(int id)
        {
            string url = $"{_url}collection/GetUserTasksCount/{id}";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<Dictionary<Project, int>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<Task>> GetUserTasks_NameLess45(int id)
        {
            string url = $"{_url}collection/GetUserTasks_NameLess45/{id}";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<Task>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<(int Id, string? Name)>> GetUserTasks_Finished2021(int id)
        {
            string url = $"{_url}collection/GetUserTasks_Finished2021/{id}";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<(int Id, string? Name)>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<(int Id, string? Name, List<User>)>> GetUsersAgeTenPlus()
        {
            string url = $"{_url}collection/GetUsersAgeTenPlus";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<(int Id, string? Name, List<User>)>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<User>> GetUsersByNameAscTaskNameLenDesc()
        {
            string url = $"{_url}collection/GetUsersByNameAscTaskNameLenDesc";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<User>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<StructForSixHomeTask> GetStructureForSixHomeTask(int id)
        {
            string url = $"{_url}collection/GetStructureForSixHomeTask/{id}";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<StructForSixHomeTask>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
        
        public async ThreadTask.Task<List<StructForSevenHomeTask>> GetStructureForSevenHomeTask()
        {
            string url = $"{_url}collection/GetStructureForSevenHomeTask";

            var response = await client.GetAsync(url);
            var responseRes = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<StructForSevenHomeTask>>(responseRes);
            else
                throw new HttpRequestException(response.StatusCode.ToString());
        }
    }
}