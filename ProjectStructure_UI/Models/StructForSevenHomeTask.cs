﻿namespace ProjectStructure_UI.Models
{
    public struct StructForSevenHomeTask
    {
        public Project Project { get; set; }
        public Task LongestTaskDescription { get; set; }
        public Task ShortestTaskName { get; set; }
        public int UsersInTeam { get; set; }
    }
}