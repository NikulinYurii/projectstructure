﻿namespace ProjectStructure_UI.Models
{
    public struct StructForSixHomeTask
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int NotDoneTask { get; set; }
        public Task LongestTask { get; set; }
    }
}