﻿using System;

namespace ProjectStructure_UI.Models.DTO
{
    public class TaskDTO 
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"ProjectId: {ProjectId}\n" +
                $"PerformerId: {PerformerId}\n" +
                $"Name: {Name}\n" +
                $"Description: {Description}\n" +
                $"State: {State}\n" +
                $"CreatedAt: {CreatedAt}\n" +
                $"FinishedAt: {FinishedAt}";
        }
    }
}