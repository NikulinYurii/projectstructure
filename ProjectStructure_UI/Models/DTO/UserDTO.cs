﻿using System;

namespace ProjectStructure_UI.Models.DTO
{
    public class UserDTO 
    {
        public int  Id { get; set; }
        public int? TeamId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"TeamId: {TeamId}\n" +
                $"FirstName: {FirstName}\n" +
                $"LastName: {LastName}\n" +
                $"Email: {Email}\n" +
                $"RegisteredAt: {RegisteredAt}\n" +
                $"BirthDay: {BirthDay}";
        }
    }
}