﻿using System;

namespace ProjectStructure_UI.Models.DTO
{
    public class TeamDTO 
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Name: {Name}\n" +
                $"CreatedAt: {CreatedAt}";
        }
    }
}