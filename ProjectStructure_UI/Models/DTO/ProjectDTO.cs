﻿using System;

namespace ProjectStructure_UI.Models.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"AuthorId: {AuthorId}\n" +
                $"TeamId: {TeamId}\n" +
                $"Name: {Name}\n" +
                $"Description: {Description}\n" +
                $"Deadline: {Deadline}\n" +
                $"CreatedAt: {CreatedAt}";
        }
    }
}