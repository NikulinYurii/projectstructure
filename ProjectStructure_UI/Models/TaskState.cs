﻿namespace ProjectStructure_UI.Models
{
    public enum TaskState
    {
        Story,
        ToDo,
        InProcess,
        ToVerify,
        Done
    }
}