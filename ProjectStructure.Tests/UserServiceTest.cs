﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using ProjectStructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.Tests
{
    public class UserServiceTest
    {
        private ProjectStructureDbContext _context;
        
        public UserServiceTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        [Fact]
        public void Create_WhenAddUserToTeam_ThenReturnTeamUsersCount()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };

            _context.Teams.AddRange(teams);
            _context.SaveChanges();

            var userService = new UserService(_context);
            userService.Create(users[0]);
            userService.Create(users[1]);

            Assert.Equal(2, _context.Teams.ToList()[0].Users.Count());
        }
    }
}
