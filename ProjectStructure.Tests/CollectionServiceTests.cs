using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using ProjectStructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ProjectStructure.Tests
{
    public class CollectionServiceTests : IDisposable
    {
        private readonly ProjectStructureDbContext _context;

        public CollectionServiceTests()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void GetAllNotDoneTasks_WhenHaveTask_ThenGetResult()
        {
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.InProcess,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"),
                    FinishedAt = null
                }
            };

            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetAllNotDoneTasks(1);

            Assert.NotEmpty(res);
        }

        [Fact]
        public void GetAllNotDoneTasks_WhenUnCorrectUserId_ThenGetError()
        {
            var collectionService = new CollectionService(_context);

            Assert.Throws<ArgumentException>(()=>collectionService.GetAllNotDoneTasks(-1));
        }

        [Fact]
        public void GetUserTasksCount_WhenAddTwoTasksForOneUser_ThenGetTwoTasks()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.ToDo,
                    CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"),
                    FinishedAt = null
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetUserTasksCount(1).Result;

            Assert.NotEmpty(res.Values);
        }

        [Fact]
        public void GetUserTasks_NameLess45_WhenAddTwoTasksForOneUser_ThenGetTwoTasks()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.ToDo,
                    CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"),
                    FinishedAt = null
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetUserTasks_NameLess45(1).Result;

            Assert.Equal(2, res.Count);
        }

        [Fact]
        public void GetUserTasks_Finished2021_WhenHawOneResult_ThenGetOneResult()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetUserTasks_Finished2021(1).Result;

            Assert.Equal(1, res.Count);
        }

        [Fact]
        public void GetUsersAgeTenPlus_WhenHaveOneTeamWhereUsersOlderThanTen_ThenGetOne()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetUsersAgeTenPlus().Result;

            Assert.Equal(1, res.Count);
        }

        [Fact]
        public void GetUsersByNameAscTaskNameLenDesc() 
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            foreach(var u in users)
            {
                u.Tasks = tasks;
            }

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var collectionService = new CollectionService(_context);
            var res = collectionService.GetUsersByNameAscTaskNameLenDesc().Result;

            Assert.Equal("Erin", res.First<User>().FirstName);
        }
    }
}
