﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using ProjectStructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ProjectStructure.Tests
{
    public class TaskServiceTest
    {
        private ProjectStructureDbContext _context;

        public TaskServiceTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectStructureDbContext>().UseInMemoryDatabase("TestDb");
            _context = new ProjectStructureDbContext(optionsBuilder.Options);
        }

        [Fact]
        public void Update()
        {
            var task = new Task
            {
                Id = 1,
                ProjectId = 1,
                UserId = 1,
                Name = "index",
                Description = "Eveniet nihil asperiores esse minima.",
                State = TaskState.InProcess,
                CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                FinishedAt = null
            };

            _context.Tasks.Add(task);
            _context.SaveChanges();

            var taskService = new TaskService(_context);

            task.State = TaskState.Done;
            task.FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00");

            taskService.Update(task);

            Assert.Equal(TaskState.Done, _context.Tasks.First(x => x.Id == 1).State);
            Assert.Equal(Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00"), _context.Tasks.First(x => x.Id == 1).FinishedAt);
        }

        [Fact]
        public void Delete()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var taskService = new TaskService(_context);
            taskService.Delete(tasks.ToList()[0]);

            Assert.Empty(_context.Tasks);
        }

        [Fact]
        public void GetAll()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var taskService = new TaskService(_context);
            var res = taskService.GetAll();

            Assert.Single(res);
        }

        [Fact]
        public void GetById()
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 3,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                },
                new Project
                {
                    Id = 3,
                    AuthorId = 3,
                    TeamId = 3,
                    ProjectName = "Village",
                    Description = "Non voluptatem voluptas libero.",
                    Deadline = Convert.ToDateTime("2021-07-24T12:07:31.9357846+00:00"),
                    CreatedAt = Convert.ToDateTime("2021-01-30T14:38:53.8838745+00:00")
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 1,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                },
                new Team
                {
                    Id = 3,
                    TeamName = "Kassulke LLC",
                    CreatedAt = Convert.ToDateTime("2019-02-21T15:47:30.3797852+00:00")
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
                }
            };

            _context.Projects.AddRange(projects);
            _context.Users.AddRange(users);
            _context.Teams.AddRange(teams);
            _context.Tasks.AddRange(tasks);
            _context.SaveChanges();

            var taskService = new TaskService(_context);
            var res = taskService.GetById(1);

            Assert.Equal(1, res.Id);
            Assert.Equal("index", res.Name);
        }

        [Fact]
        public void Create()
        {
            var task = new Task
            {
                Id = 1,
                ProjectId = 1,
                UserId = 1,
                Name = "index",
                Description = "Eveniet nihil asperiores esse minima.",
                State = TaskState.Done,
                CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                FinishedAt = Convert.ToDateTime("2021-10-31T09:39:16.9900799+00:00")
            };

            var taskService = new TaskService(_context);
            taskService.Create(task);

            Assert.Single(_context.Tasks);
        }
    }
}
