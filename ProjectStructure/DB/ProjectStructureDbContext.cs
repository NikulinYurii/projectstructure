﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure
{
    public class ProjectStructureDbContext : DbContext
    {
        public ProjectStructureDbContext(DbContextOptions<ProjectStructureDbContext> options)
            : base(options)
        {
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    TeamName = "Denesik - Greenfelder",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                },
                new Team
                {
                    Id = 2,
                    TeamName = "Durgan Group",
                    CreatedAt = Convert.ToDateTime("2017-03-31T02:29:28.3740504+00:00")
                },
                new Team
                {
                    Id = 3,
                    TeamName = "Kassulke LLC",
                    CreatedAt = Convert.ToDateTime("2019-02-21T15:47:30.3797852+00:00")
                },
                new Team
                {
                    Id = 4,
                    TeamName = "Harris LLC",
                    CreatedAt = Convert.ToDateTime("2018-08-28T08:18:46.4160342+00:00")
                },
                new Team
                {
                    Id = 5,
                    TeamName = "Mitchell Inc",
                    CreatedAt = Convert.ToDateTime("2016-10-05T07:57:02.8427653+00:00")
                },
                new Team
                {
                    Id = 6,
                    TeamName = "Smitham Group",
                    CreatedAt = Convert.ToDateTime("2019-08-25T15:48:06.062331+00:00")
                },
                new Team
                {
                    Id = 7,
                    TeamName = "Kutch - Roberts",
                    CreatedAt = Convert.ToDateTime("2016-10-31T05:05:15.1076578+00:00")
                },
                new Team
                {
                    Id = 8,
                    TeamName = "Parisian Group",
                    CreatedAt = Convert.ToDateTime("2016-07-17T01:34:55.0917082+00:00")
                },
                new Team
                {
                    Id = 9,
                    TeamName = "Schiller Group",
                    CreatedAt = Convert.ToDateTime("2020-10-05T01:20:14.1953926+00:00")
                },
                new Team
                {
                    Id = 10,
                    TeamName = "Littel, Turcotte and Muller",
                    CreatedAt = Convert.ToDateTime("2018-10-19T14:54:27.5549549+00:00")
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Marcos",
                    LastName = "Stamm",
                    UserEmail = "Marcos.Stamm70@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-01-18T04:18:32.5952402+00:00"),
                    BirthDay = Convert.ToDateTime("1981-07-19T08:21:52.6291502+00:00")
                },
                new User
                {
                    Id = 2,
                    TeamId = 1,
                    FirstName = "Anne",
                    LastName = "Collier",
                    UserEmail = "Anne.Collier@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-04-24T03:13:17.101291+00:00"),
                    BirthDay = Convert.ToDateTime("1987-03-12T09:11:01.3536142+00:00")
                },
                new User
                {
                    Id = 3,
                    TeamId = 2,
                    FirstName = "Erin",
                    LastName = "Pacocha",
                    UserEmail = "Erin.Pacocha14@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2020-08-14T06:01:52.6925066+00:00"),
                    BirthDay = Convert.ToDateTime("2005-03-07T01:42:11.4669734+00:00")
                },
                new User
                {
                    Id = 4,
                    TeamId = 2,
                    FirstName = "Jennifer",
                    LastName = "Haag",
                    UserEmail = "Jennifer37@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2019-11-14T18:01:12.835198+00:00"),
                    BirthDay = Convert.ToDateTime("1963-01-27T12:03:01.0831205+00:00")
                },
                new User
                {
                    Id = 5,
                    TeamId = 3,
                    FirstName = "Elizabeth",
                    LastName = "Koepp",
                    UserEmail = "Elizabeth94@hotmail.com",
                    RegisteredAt = Convert.ToDateTime("2021-06-11T19:05:36.352357+00:00"),
                    BirthDay = Convert.ToDateTime("1965-05-26T18:27:15.2255076+00:00")
                },
                new User
                {
                    Id = 6,
                    TeamId = 3,
                    FirstName = "Sadie",
                    LastName = "Bernhard",
                    UserEmail = "adie15@yahoo.com",
                    RegisteredAt = Convert.ToDateTime("2021-04-15T21:08:32.6239838+00:00"),
                    BirthDay = Convert.ToDateTime("2010-12-05T21:48:07.2846901+00:00")
                }
            };

            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 1,
                    ProjectName = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = Convert.ToDateTime("2021-08-03T01:08:10.3228394+00:00"),
                    CreatedAt = Convert.ToDateTime("2019-07-17T06:42:48.376246+00:00")
                },
                new Project
                {
                    Id = 2,
                    AuthorId = 2,
                    TeamId = 2,
                    ProjectName = "backing up Handcrafted Fresh Shoes challenge",
                    Description = "Et doloribus et temporibus.",
                    Deadline = Convert.ToDateTime("2021-09-12T19:17:47.2335223+00:00"),
                    CreatedAt = Convert.ToDateTime("2020-08-25T17:49:50.4518054+00:00")
                },
                new Project
                {
                    Id = 3,
                    AuthorId = 3,
                    TeamId = 3,
                    ProjectName = "Village",
                    Description = "Non voluptatem voluptas libero.",
                    Deadline = Convert.ToDateTime("2021-07-24T12:07:31.9357846+00:00"),
                    CreatedAt = Convert.ToDateTime("2021-01-30T14:38:53.8838745+00:00")
                },
                new Project
                {
                    Id = 4,
                    AuthorId = 4,
                    TeamId = 4,
                    ProjectName = "Dam",
                    Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
                    Deadline = Convert.ToDateTime("2021-07-21T08:32:51.3354654+00:00"),
                    CreatedAt = Convert.ToDateTime("2020-03-15T20:33:15.6731141+00:00")
                }
            };

            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId =1,
                    UserId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2017-10-31T09:39:16.9900799+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId =1,
                    UserId = 1,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.ToDo,
                    CreatedAt = Convert.ToDateTime("2020-05-15T22:50:46.0860832+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 3,
                    ProjectId =1,
                    UserId = 1,
                    Name = "product Direct utilize",
                    Description = "Eum a eum.",
                    State = TaskState.InProcess,
                    CreatedAt = Convert.ToDateTime("2019-02-15T15:06:52.0600666+00:00"),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 4,
                    ProjectId =1,
                    UserId = 1,
                    Name = "bypass",
                    Description = "Sint voluptatem quas.",
                    State = TaskState.Done,
                    CreatedAt = Convert.ToDateTime("2017-08-16T06:13:44.5773845+00:00"),
                    FinishedAt = Convert.ToDateTime("2018-10-19T00:58:34.8045103+00:00")
                },
                new Task
                {
                    Id = 5,
                    ProjectId =1,
                    UserId = 1,
                    Name = "withdrawal contextually-based",
                    Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
                    State = TaskState.Story,
                    CreatedAt = Convert.ToDateTime("2018-10-19T00:58:34.8045103+00:00"),
                    FinishedAt = null
                }
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(modelBuilder);
        }
    }
}