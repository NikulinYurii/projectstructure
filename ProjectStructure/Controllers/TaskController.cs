﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;
using ProjectStructure.Services;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Controllers
{
    [ApiController]
    [Route("task")]
    public class TaskController : ControllerBase
    {
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        private readonly TaskService _taskService;
        private readonly IMapper _mapper;
        
        public TaskController(ProjectService projectService, UserService userService, TaskService taskService, IMapper mapper)
        {
            _projectService = projectService;
            _userService = userService;
            _taskService = taskService;
            _mapper = mapper;
        }

        [HttpDelete("dell/{id}")]
        public async ThreadTask.Task<ActionResult> DellTask(int id)
        {
            try
            {
                await _projectService.Delete(await _projectService.GetById(id));
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound("Задачу з таким Id не знайдено");
            }
        }

        [HttpGet("get/all")]
        public async ThreadTask.Task<ActionResult<List<TaskDTO>>> GetAll()
        {
            var tasks = await _taskService.GetAll();
            List <TaskDTO> res = tasks
                .Select(p => _mapper.Map<TaskDTO>(p)
                ).ToList();
            return Ok(res);
        }

        [HttpGet("get/{id}")]
        public async ThreadTask.Task<ActionResult<TaskDTO>> GetById(int id)
        {
            try
            {
                return Ok(_mapper.Map<TaskDTO>(await _taskService.GetById(id)));
            }
            catch (Exception e)
            {
                return NotFound("Задачу з таким Id не знайдено");
            }
        }

        [HttpPost("add")]
        public async ThreadTask.Task<ActionResult<TaskDTO>> AddProject(TaskDTO taskDto)
        {
            try
            {
                var task = new Task()
                {
                    Id = taskDto.Id,
                    Project = await _projectService.GetById(taskDto.ProjectId),
                    Performer = await _userService.GetById(taskDto.PerformerId),
                    Name = taskDto.Name,
                    Description = taskDto.Description,
                    State = taskDto.State,
                    CreatedAt = taskDto.CreatedAt,
                    FinishedAt = taskDto.FinishedAt
                };
                await _taskService.Create(task);
                return Created("ProjectStructure/Models/TaskDTO", _mapper.Map<TaskDTO>(task));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("update/{id}")]
        public async ThreadTask.Task<ActionResult<TaskDTO>> UpdateTask(TaskDTO taskDto)
        {
            try
            {
                var task = new Task()
                {
                    Id = taskDto.Id,
                    Project = await _projectService.GetById(taskDto.ProjectId),
                    Performer = await _userService.GetById(taskDto.PerformerId),
                    Name = taskDto.Name,
                    Description = taskDto.Description,
                    State = taskDto.State,
                    CreatedAt = taskDto.CreatedAt,
                    FinishedAt = taskDto.FinishedAt
                };
                await _taskService.Update(task);
                return Created("ProjectStructure/Models/TaskDTO", _mapper.Map<TaskDTO>(task));
            }
            catch (Exception e)
            {
                return NotFound("Задачу з таким Id не знайдено");
            }
        }
    }
}