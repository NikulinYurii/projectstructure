﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;
using ProjectStructure.Services;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Controllers
{
    [ApiController]
    [Route("project")]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        private readonly TeamService _teamService;
        private readonly IMapper _mapper;

        public ProjectController(ProjectService projectService, UserService userService, TeamService teamService, IMapper mapper)
        {
            _projectService = projectService;
            _userService = userService;
            _teamService = teamService;
            _mapper = mapper;
        }

        [HttpDelete("dell/{id}")]
        public async ThreadTask.Task<ActionResult> DellProject(int id)
        {
            try
            {
                await _projectService.Delete(await _projectService.GetById(id));
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound("Проект з таким Id не знайдено");
            }
        }

        [HttpGet("get/all")]
        public async ThreadTask.Task<ActionResult<List<ProjectDTO>>> GetAll()
        {
            List<Project> projects = await _projectService.GetAll();
            List<ProjectDTO> res = projects
                .Select(p => _mapper.Map<ProjectDTO>(p)
            ).ToList();
            
            return Ok(res);
        }

        [HttpGet("get/{id}")]
        public async ThreadTask.Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            try
            {
                return Ok(_mapper.Map<ProjectDTO>(await _projectService.GetById(id)));
            }
            catch (Exception e)
            {
                return NotFound("Проект з таким Id не знайдено");
            }
        }

        [HttpPost("add")]
        public async ThreadTask.Task<ActionResult<ProjectDTO>> AddProject(ProjectDTO projectDto)
        {
            try
            {
                var project = new Project()
                {
                    Id = projectDto.Id,
                    Author = await _userService.GetById(projectDto.AuthorId),
                    ProjectTeam = await _teamService.GetById(projectDto.TeamId),
                    ProjectName = projectDto.Name,
                    Description = projectDto.Description,
                    Deadline = projectDto.Deadline,
                    CreatedAt = projectDto.CreatedAt
                };
                await _projectService.Create(project);
                return Created("ProjectStructure/Models/ProjectDTO", _mapper.Map<ProjectDTO>(project));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("update/{id}")]
        public async ThreadTask.Task<ActionResult<ProjectDTO>> UpdateProduct(ProjectDTO projectDto)
        {
            try
            {
                var project = new Project()
                {
                    Id = projectDto.Id,
                    Author = await _userService.GetById(projectDto.AuthorId),
                    ProjectTeam = await _teamService.GetById(projectDto.TeamId),
                    ProjectName = projectDto.Name,
                    Description = projectDto.Description,
                    Deadline = projectDto.Deadline,
                    CreatedAt = projectDto.CreatedAt
                };
                await _projectService.Update(project);
                return Created("ProjectStructure/Models/ProjectDTO", _mapper.Map<ProjectDTO>(project));
            }
            catch (Exception e)
            {
                return NotFound("Проект з таким Id не знайдено");
            }
        }
    }
}