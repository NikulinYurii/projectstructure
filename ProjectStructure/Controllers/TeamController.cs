﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;
using ProjectStructure.Services;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Controllers
{
    [ApiController]
    [Route("team")]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;
        private readonly IMapper _mapper;

        public TeamController(TeamService teamService, IMapper mapper)
        {
            _teamService = teamService;
            _mapper = mapper;
        }

        [HttpDelete("dell/{id}")]
        public async ThreadTask.Task<ActionResult> DellTeam(int id)
        {
            try
            {
                await _teamService.Delete(await _teamService.GetById(id));
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound("Команда з таким Id не знайдено");
            }
        }

        [HttpGet("get/all")]
        public async ThreadTask.Task<ActionResult<List<TeamDTO>>> GetAll()
        {
            var teams = await _teamService.GetAll();
            List<TeamDTO> res = teams
                .Select(p => _mapper.Map<TeamDTO>(p)
                ).ToList();
            return Ok(res);
        }

        [HttpGet("get/{id}")]
        public async ThreadTask.Task<ActionResult<TeamDTO>> GetById(int id)
        {
            try
            {
                return Ok(_mapper.Map<TeamDTO>(await _teamService.GetById(id)));
            }
            catch (Exception e)
            {
                return NotFound("Команда з таким Id не знайдено");
            }
        }

        [HttpPost("add")]
        public async ThreadTask.Task<ActionResult<TeamDTO>> AddTeam(TeamDTO teamDto)
        {
            try
            {
                var team = new Team()
                {
                    Id = teamDto.Id,
                    TeamName = teamDto.Name,
                    CreatedAt = teamDto.CreatedAt
                };
                await _teamService.Create(team);
                return Created("ProjectStructure/Models/TeamDTO", _mapper.Map<TeamDTO>(team));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("update/{id}")]
        public async ThreadTask.Task<ActionResult<TeamDTO>> UpdateTeam(TeamDTO teamDto)
        {
            try
            {
                var team = new Team()
                {
                    Id = teamDto.Id,
                    TeamName = teamDto.Name,
                    CreatedAt = teamDto.CreatedAt
                };
                await _teamService.Update(team);
                return Created("ProjectStructure/Models/TeamDTO", _mapper.Map<TeamDTO>(team));
            }
            catch (Exception e)
            {
                return NotFound("Команда з таким Id не знайдено");
            }
        }
    }
}