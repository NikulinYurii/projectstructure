﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Services;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Controllers
{
    [ApiController]
    [Route("collection")]
    public class CollectionController : ControllerBase
    {
        private readonly CollectionService _collectionService;

        public CollectionController(CollectionService collectionService)
        {
            _collectionService = collectionService;
        }
        
        [HttpGet("GetAllNotDoneTasks/{id}")]
        public async ThreadTask.Task<ActionResult<List<Task>>> GetAllNotDoneTasks(int id)
        {
            return Ok(await _collectionService.GetAllNotDoneTasks(id));
        }

        [HttpGet("GetUserTasksCount/{id}")]
        public async ThreadTask.Task<ActionResult<Dictionary<Project, int>>> GetUserTasksCount(int id)
        {
            return Ok(await _collectionService.GetUserTasksCount(id));
        }
        
        [HttpGet("GetUserTasks_NameLess45/{id}")]
        public async ThreadTask.Task<ActionResult<List<Task>>> GetUserTasks_NameLess45(int id)
        {
            return Ok(await _collectionService.GetUserTasks_NameLess45(id));
        }
        
        [HttpGet("GetUserTasks_Finished2021/{id}")]
        public async ThreadTask.Task<ActionResult<List<(int Id, string? Name)>>> GetUserTasks_Finished2021(int id)
        {
            return Ok(await _collectionService.GetUserTasks_Finished2021(id));
        }
        
        [HttpGet("GetUsersAgeTenPlus")]
        public async ThreadTask.Task<ActionResult<List<(int Id, string? Name, List<User>)>>> GetUsersAgeTenPlus()
        {
            return Ok(await _collectionService.GetUsersAgeTenPlus());
        }
        
        [HttpGet("GetUsersByNameAscTaskNameLenDesc")]
        public async ThreadTask.Task<ActionResult<List<User>>> GetUsersByNameAscTaskNameLenDesc()
        {
            return Ok(await _collectionService.GetUsersByNameAscTaskNameLenDesc());
        }
        
        [HttpGet("GetStructureForSixHomeTask/{id}")]
        public async ThreadTask.Task<ActionResult<CollectionService.StructForSixHomeTask>> GetStructureForSixHomeTask(int id)
        {
            return Ok(await _collectionService.GetStructureForSixHomeTask(id));
        }
        
        [HttpGet("GetStructureForSevenHomeTask")]
        public async ThreadTask.Task<ActionResult<List<CollectionService.StructForSevenHomeTask>>> GetStructureForSevenHomeTask()
        {
            return Ok(await _collectionService.GetStructureForSevenHomeTask());
        }
    }
}