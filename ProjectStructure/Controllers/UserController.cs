﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;
using ProjectStructure.Services;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Controllers
{
    [ApiController]
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly TeamService _teamService;
        private readonly IMapper _mapper;

        public UserController(UserService userService, TeamService teamService, IMapper mapper)
        {
            _userService = userService;
            _teamService = teamService;
            _mapper = mapper;
        }

        [HttpDelete("dell/{id}")]
        public async ThreadTask.Task<ActionResult> DellUser(int id)
        {
            try
            {
                await _userService.Delete(await _userService.GetById(id));
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound("Користувач з таким Id не знайдено");
            }
        }

        [HttpGet("get/all")]
        public async ThreadTask.Task<ActionResult<List<UserDTO>>> GetAll()
        {
            var users = await _userService.GetAll();
            List <UserDTO> res = users
                .Select(p => _mapper.Map<UserDTO>(p)
                ).ToList();
            return Ok(res);
        }

        [HttpGet("get/{id}")]
        public async ThreadTask.Task<ActionResult<UserDTO>> GetById(int id)
        {
            try
            {
                return Ok(_mapper.Map<UserDTO>(await _userService.GetById(id)));
            }
            catch (Exception e)
            {
                return NotFound("Користувач з таким Id не знайдено");
            }
        }

        [HttpPost("add")]
        public async ThreadTask.Task<ActionResult<UserDTO>> AddUser(UserDTO userDto)
        {
            try
            {
                var user = new User()
                {
                    Id = userDto.Id,
                    UserTeam = await _teamService.GetById(userDto.TeamId),
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    UserEmail = userDto.Email,
                    RegisteredAt = userDto.RegisteredAt,
                    BirthDay = userDto.BirthDay
                };
                await _userService.Create(user);
                return Created("ProjectStructure/Models/UserDTO", _mapper.Map<UserDTO>(user));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("update/{id}")]
        public async ThreadTask.Task<ActionResult<UserDTO>> UpdateUser(UserDTO userDto)
        {
            try
            {
                var user = new User()
                {
                    Id = userDto.Id,
                    UserTeam = await _teamService.GetById(userDto.TeamId),
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    UserEmail = userDto.Email,
                    RegisteredAt = userDto.RegisteredAt,
                    BirthDay = userDto.BirthDay
                };
                await _userService.Update(user);
                return Created("ProjectStructure/Models/UserDTO", _mapper.Map<UserDTO>(user));
            }
            catch (Exception e)
            {
                return NotFound("Користувач з таким Id не знайдено");
            }
        }
    }
}