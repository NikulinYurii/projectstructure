﻿using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Models;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Services
{
    public class TeamService
    {
        private ProjectStructureDbContext _context;

        public TeamService(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async ThreadTask.Task Delete(Team team)
        {
            await ThreadTask.Task.Run(() => _context.Teams.Remove(team));
            await _context.SaveChangesAsync();
        }
        
        public async ThreadTask.Task<List<Team>> GetAll()
        {
            return await ThreadTask.Task.Run(() => _context.Teams.ToList());
        }

        public async ThreadTask.Task<Team> GetById(int? id)
        {
            if (id == null)
            {
                return null;
            }

            return await _context.Teams.FindAsync(id);
        }

        public async ThreadTask.Task Create(Team team)
        {
            await _context.Teams.AddAsync(team);
            await _context.SaveChangesAsync();
        }

        public async ThreadTask.Task Update(Team team)
        {
            await ThreadTask.Task.Run(() => _context.Teams.Update(team));
            await _context.SaveChangesAsync();
        }
    }
}