﻿using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Models;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Services
{
    public class ProjectService
    {
        private ProjectStructureDbContext _context;

        public ProjectService(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async ThreadTask.Task Delete(Project project)
        {
            await ThreadTask.Task.Run(() => _context.Projects.Remove(project));
            await _context.SaveChangesAsync();
        }
        
        public async ThreadTask.Task<List<Project>> GetAll()
        {
            return await ThreadTask.Task.Run(() => _context.Projects.ToList());
        }

        public async ThreadTask.Task<Project> GetById(int id)
        {
            return await _context.Projects.FindAsync(id);
        }

        public async ThreadTask.Task Create(Project project)
        {
            await _context.Projects.AddAsync(project);
            await _context.SaveChangesAsync();
        }

        public async ThreadTask.Task Update(Project project)
        {
            await ThreadTask.Task.Run(() => _context.Projects.Update(project));
            await _context.SaveChangesAsync();
        }
    }
}