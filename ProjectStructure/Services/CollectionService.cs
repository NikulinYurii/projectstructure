﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Models;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Services
{
    public class CollectionService
    {
        private ProjectStructureDbContext _context;

        public CollectionService(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async ThreadTask.Task<List<Task>> GetAllNotDoneTasks(int userId)
        {
            if(userId < 0)
                throw new ArgumentException("userId can be > 0");

            return await ThreadTask.Task.Run(()=> _context.Tasks.ToList()
                .Where(t => t.UserId == userId &&
                            t.State != TaskState.Done &&
                            t.FinishedAt == null)
                .ToList());
        }

        //Отримати кількість тасків у проекті конкретного користувача (по id)
        //(словник, де key буде проект, а value кількість тасків).
        public async ThreadTask.Task<Dictionary<Project, int>> GetUserTasksCount(int userId)
        {
            return await ThreadTask.Task.Run(() => _context.Tasks.ToList()
                .Where(x => x.Performer.Id == userId)
                .GroupBy(x => x.Project)
                .ToDictionary(
                    x => x.Key,
                    x => x.Count()));
        }

        //Отримати список тасків, призначених для конкретного користувача (по id),
        //де name таска <45 символів (колекція з тасків).
        public async ThreadTask.Task<List<Task>> GetUserTasks_NameLess45(int userId)
        {
            return await ThreadTask.Task.Run(() => _context.Tasks.ToList()
                .Where(t => t.Name.Length < 45 && t.Performer.Id == userId)
                .ToList());
        }

        //Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021)
        //році для конкретного користувача (по id).
        public async ThreadTask.Task<List<(int Id, string? Name)>> GetUserTasks_Finished2021(int userId)
        {
            return await ThreadTask.Task.Run(() => _context.Tasks.ToList()
                .Where(t => t.FinishedAt.HasValue
                            && t.FinishedAt.Value.Year == 2021
                            //&& t.State == TaskState.Done
                            && t.Performer.Id == userId)
                .Select(t => (t.Id, t.Name))
                .ToList());
        }

        //Отримати список (id, ім'я команди і список користувачів) з колекції команд,
        //учасники яких старші 10 років, відсортованих за датою реєстрації користувача
        //за спаданням, а також згрупованих по командах.
        public async ThreadTask.Task<List<(int Id, string? Name, List<User>)>> GetUsersAgeTenPlus()
        {
            return await ThreadTask.Task.Run(() => _context.Teams.ToList()
                .Where(t => t.Users
                    .Count(u => (DateTime.Now.Year - u.BirthDay.Year) > 10) == t.Users.Count)
                .GroupBy(t => t)
                .Select(t => (
                    t.Key.Id,
                    t.Key.TeamName,
                    t.Key.Users.OrderByDescending(u => u.RegisteredAt).ToList()))
                .ToList());
        }

        //Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими
        //tasks по довжині name (за спаданням).
        public async ThreadTask.Task<List<User>> GetUsersByNameAscTaskNameLenDesc()
        {
            return await ThreadTask.Task.Run(() => _context.Users.ToList()
                .OrderBy(u => u.FirstName)
                .Select(u => new User()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    UserEmail = u.UserEmail,
                    BirthDay = u.BirthDay,
                    RegisteredAt = u.RegisteredAt,
                    UserTeam = u.UserTeam,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                }).ToList());
        }

        public async ThreadTask.Task<StructForSixHomeTask> GetStructureForSixHomeTask(int userId)
        {
            return await ThreadTask.Task.Run(() => _context.Projects.ToList()
                .Where(p => p.Author.Id == userId)
                .OrderByDescending(p => p.CreatedAt)
                .Select(x => new StructForSixHomeTask()
                {
                    User = x.Author,
                    LastProject = x,
                    LastProjectTasksCount = x.ProjectTasks.Count,
                    NotDoneTask = x.ProjectTasks
                        .Where(t => t.Performer.Id == userId)
                        .Count(t => t.FinishedAt.HasValue == false),
                    LongestTask = x.ProjectTasks
                        .Where(t => t.Performer.Id == userId)
                        .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                        .FirstOrDefault()
                }).FirstOrDefault());
        }

        public async ThreadTask.Task<List<StructForSevenHomeTask>> GetStructureForSevenHomeTask()
        {
            return await ThreadTask.Task.Run(() => _context.Projects.ToList()
                .Select(x => new StructForSevenHomeTask()
                {
                    Project = x,
                    LongestTaskDescription = x.ProjectTasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestTaskName = x.ProjectTasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault(),
                    UsersInTeam = x
                        .ProjectTeam
                        .Users
                        .Count()
                }).ToList());
        }

        public struct StructForSixHomeTask
        {
            public User User { get; set; }
            public Project LastProject { get; set; }
            public int LastProjectTasksCount { get; set; }
            public int NotDoneTask { get; set; }
            public Task LongestTask { get; set; }
        }

        public struct StructForSevenHomeTask
        {
            public Project Project { get; set; }
            public Task LongestTaskDescription { get; set; }
            public Task ShortestTaskName { get; set; }
            public int UsersInTeam { get; set; }
        }
    }
}
