﻿using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Models;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Services
{
    public class UserService
    {
        private ProjectStructureDbContext _context;

        public UserService(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async ThreadTask.Task Delete(User user)
        {
            await ThreadTask.Task.Run(() => _context.Users.Remove(user));
            await _context.SaveChangesAsync();
        }
        
        public async ThreadTask.Task<List<User>> GetAll()
        {
            return await ThreadTask.Task.Run(() => _context.Users.ToList());
        }

        public async ThreadTask.Task<User> GetById(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async ThreadTask.Task Create(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async ThreadTask.Task Update(User user)
        {
            await ThreadTask.Task.Run(() => _context.Users.Update(user));
            await _context.SaveChangesAsync();
        }
    }
}