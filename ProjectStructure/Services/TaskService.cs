﻿using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Models;
using ThreadTask = System.Threading.Tasks;

namespace ProjectStructure.Services
{
    public class TaskService
    {
        private ProjectStructureDbContext _context;

        public TaskService(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async ThreadTask.Task Delete(Task task)
        {
            await ThreadTask.Task.Run(() => _context.Tasks.Remove(task));
            await _context.SaveChangesAsync();
        }
        
        public async ThreadTask.Task<List<Task>> GetAll()
        {
            return await ThreadTask.Task.Run(() => _context.Tasks.ToList());
        }

        public async ThreadTask.Task<Task> GetById(int id)
        {
            return await _context.Tasks.FindAsync(id);
        }

        public async ThreadTask.Task Create(Task task)
        {
            await _context.Tasks.AddAsync(task);
            await _context.SaveChangesAsync();
        }

        public async ThreadTask.Task Update(Task task)
        {
            await ThreadTask.Task.Run(() => _context.Tasks.Update(task));
            await _context.SaveChangesAsync();
        }
    }
}