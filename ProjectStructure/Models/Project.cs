﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Models
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        public int AuthorId { get; set; }
        public User Author { get; set; }
        [Required]
        public int TeamId { get; set; }
        public Team ProjectTeam { get; set; }
        public string? ProjectName { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<Task> ProjectTasks { get; set; }

        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Author: {Author.ToString()}\n" +
                $"Team: {ProjectTeam.ToString()}\n" +
                $"Name: {ProjectName}\n" +
                $"Description: {Description}\n" +
                $"Deadline: {Deadline}\n" +
                $"CreatedAt: {CreatedAt}";
        }
    }
}