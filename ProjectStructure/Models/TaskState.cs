﻿namespace ProjectStructure.Models
{
    public enum TaskState
    {
        Story,
        ToDo,
        InProcess,
        ToVerify,
        Done
    }
}