﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Models
{
    public class User
    {
        public int  Id { get; set; }
        public int TeamId { get; set; }
        public Team? UserTeam { get; set; }
        [Required]
        [MaxLength(40)]
        public string? FirstName { get; set; }
        [MaxLength(40)]
        public string? LastName { get; set; }
        [MaxLength(140)]
        public string? UserEmail { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public List<Task> Tasks { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"TeamId: {UserTeam.ToString()}\n" +
                $"FirstName: {FirstName}\n" +
                $"LastName: {LastName}\n" +
                $"Email: {UserEmail}\n" +
                $"RegisteredAt: {RegisteredAt}\n" +
                $"BirthDay: {BirthDay}";
        }
    }
}