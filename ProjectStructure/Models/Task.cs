﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Models
{
    public class Task
    {
        public int Id { get; set; }
        [Required]
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        [Required]
        public int UserId { get; set; }
        public User Performer { get; set; }
        [MaxLength(140)]
        public string? Name { get; set; }
        public string? Description { get; set; }
        [Required]
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Project: {Project.ToString()}\n" +
                $"Performer: {Performer.ToString()}\n" +
                $"Name: {Name}\n" +
                $"Description: {Description}\n" +
                $"State: {State}\n" +
                $"CreatedAt: {CreatedAt}\n" +
                $"FinishedAt: {FinishedAt}";
        }
    }
}