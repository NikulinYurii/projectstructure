﻿using AutoMapper;
using ProjectStructure.Models;
using ProjectStructure.Models.DTO;

namespace ProjectStructure
{
    public class AutoMapperSettings : Profile
    {
        public AutoMapperSettings()  
        {  
            CreateMap<Project, ProjectDTO>()
                .ForMember(dest=>dest.AuthorId, 
                    opt=>opt.MapFrom(
                        src=>src.Author.Id))
                .ForMember(dest=>dest.TeamId,
                    opt=>opt.MapFrom(
                        src=>src.ProjectTeam.Id));
            
            CreateMap<Task, TaskDTO>()
                .ForMember(dest=>dest.ProjectId, 
                opt=>opt.MapFrom(
                    src=>src.Project.Id))
                .ForMember(dest=>dest.PerformerId,
                    opt=>opt.MapFrom(
                        src=>src.Performer.Id));  
            
            CreateMap<Team, TeamDTO>(); 
            CreateMap<User, UserDTO>()
                .ForMember(dest=>dest.TeamId, 
                opt=>opt.MapFrom(
                    src=>src.UserTeam.Id)); 
        }  
    }
}